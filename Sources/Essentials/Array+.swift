import Foundation

public extension Collection {
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
    subscript(safeIdxs idxArr: [Index] ) -> [Element] {
        return idxArr.compactMap{ self[safe: $0] }
    }
    
    subscript(safeIdxs idxArr: CountableClosedRange<Int> ) -> [Element] {
        return idxArr.compactMap{ $0 as? Index }.compactMap{ self[ safe: $0] }
    }
    
    subscript(safeIdxs idxArr: CountableRange<Int> ) -> [Element] {
        return idxArr.compactMap{ $0 as? Index }.compactMap{ self[ safe: $0 ] }
    }
    
    subscript (infinityIdx idx: Index) -> Element where Index == Int {
        return self[ abs(idx) % self.count ]
    }
}


public extension Sequence {
    func toDictionary<Key: Hashable, Value>(block: (Element)->(Value)) -> [Key:Value] where Key == Self.Element {
        self.toDictionary(key: \.self, block: block)
    }
    
    func toDictionary<Key: Hashable, Value>(key: KeyPath<Element, Key>, block: (Element)->(Value)) -> [Key:Value] {
        var dict: [Key:Value] = [:]
        
        for element in self {
            let key = element[keyPath: key]
            let value = block(element)
            
            dict[key] = value
        }
        
        return dict
    }
}

public extension Array {
    func mapTo<Element2,JointType>(_ arr: Array<Element2>, mapper: (Element?,Element2?)->(JointType)) -> [JointType]
    {
        var result = [JointType]()
        
        let maxIdx = Swift.max(count, arr.count) - 1
        if maxIdx > 0 {
            for i in 0 ... maxIdx {
                var item1 : Element?
                var item2 : Element2?
                
                if self.count > i {
                    item1 = self[i]
                }
                
                if arr.count > i {
                    item2 = arr[i]
                }
                
                result.append(mapper(item1, item2))
            }
        }
        return result
    }
    
    func sum<T: Numeric>(_ getter: (Element)->(T)) -> T {
        return self.map(getter).reduce(0, +)
    }
    
    func join(_ getter: (Element)->(String)) -> String {
        return self.map(getter).reduce("", +)
    }
    
    func splitBy(_ chunkSize: Int) -> [[Element]] {
        return stride(from: 0, to: self.count, by: chunkSize).map {
            Array(self[$0..<Swift.min($0 + chunkSize, self.count)])
        }
    }
    
    mutating func removeFirst(where predicate: (Element)->(Bool)) {
        if let idx = self.firstIndex(where: predicate) {
            remove(at: idx)
        }
    }
    
    func recursiveFlatMap(_ getter: (Element)->([Element])) -> [Element] {
        var results = [Element]()
        results.append(contentsOf: self)
        
        for item in self {
            results.append(contentsOf: getter(item).recursiveFlatMap(getter) )
        }
        
        return results
    }
}


public extension Array where Element: Any {
    /// returns NEW array!
    func inserting(contentsOf collection: [Element], at: Int) -> [Element] {
        var temp: [Element] = []
        temp.append(contentsOf: self)
        temp.insert(contentsOf: collection, at: at)
        return temp
    }
    
    /// returns NEW array!
    func inserting(_ newElement: Element, at: Int) -> [Element] {
        var temp: [Element] = []
        temp.append(contentsOf: self)
        temp.insert(newElement, at: at)
        return temp
    }
    
    /// returns NEW array!
    func appending(_ newItem: Element) -> [Element] {
        var temp: [Element] = []
        temp.append(contentsOf: self)
        temp.append(newItem)
        return temp
    }
    
    /// returns NEW array!
    func appending(contentsOf newItems: [Element]) -> [Element] {
        var temp: [Element] = []
        temp.append(contentsOf: self )
        temp.append(contentsOf: newItems)
        return temp
    }
  
    static func + (me: Self, newItem: Element) -> Self {
        me.appending(newItem)
    }
  
  @discardableResult
  static func += (me: inout Self, newItem: Element) -> Self {
    me.append(newItem)
    return me
  }
}

public extension Array where Element: Hashable {
    ///Returns Distinct elements but without change position of elements
    func distinct() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.distinct()
    }
}

public extension Array where Element : Equatable {
    var distinctAlter: [Element] {
        var uniqueValues: [Element] = []
        forEach { item in
            if !uniqueValues.contains(item) {
                uniqueValues += [item]
            }
        }
        return uniqueValues
    }
}

public extension Array {
    func chunked(by size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size > 0 ? size : 1).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
    
    func chunked(into: Int) -> [[Element]] {
        return chunked(by: self.count / into )
    }
}

// Fix of horrible swift naming
public extension Array {
    func first(_ count: Int) -> ArraySlice<Element> {
        return self.suffix(count)
    }
    
    func last(_ count: Int) -> ArraySlice<Element> {
        return self.prefix(count)
    }
}
