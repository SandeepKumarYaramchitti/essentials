//
//  File.swift
//  
//
//  Created by Loki on 14.07.2022.
//

import Foundation

@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public extension Result {
    func wait() async ->  Self {
        return self
    }
}

@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public actor F<T> {
    let task : Task<T, Error>
    public init(block: @escaping () -> Result<T,Error>) {
        self.task = Task {
            try await block().wait().get()
        }
    }
}

@available(macOS 10.15.0, *)
@available(iOS 13.0, *)
public func f<T>(block: @escaping () -> Result<T,Error>) async -> Result<T,Error> {
    let f = F { block() }
    return await f.task.result
}
