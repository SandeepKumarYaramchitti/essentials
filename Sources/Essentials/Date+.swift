//
//  File 2.swift
//  
//
//  Created by UKS on 21.12.2021.
//

import Foundation

public extension Date {
    func asString(_ template: String? = nil) -> String {
        if let template = template {
            let df = DateFormatter.with(template: template)
            
            return df.string(from: self)
        }
        else {
            return globalDateFormatter.string(from: self)
        }
    }
    
    static func fromString(strDate: String, _ template: String? = nil) -> Date? {
        if let template = template {
            let df = DateFormatter.with(template: template)
            
            return df.date(from: strDate)
        }
        else {
            return globalDateFormatter.date(from: strDate)
        }
    }
}

fileprivate let globalDateFormatter: DateFormatter = DateFormatter.with(template: "y-M-dd")

public extension DateFormatter {
    static func with(template: String ) -> DateFormatter {
        let df = DateFormatter()
        df.dateFormat = template
        return df
    }
}

public extension Date {
    func plus(seconds: Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
    
    func plus(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func plus(mins: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: mins, to: self)!
    }
    
    func plus(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
}
