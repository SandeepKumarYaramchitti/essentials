//
//  File.swift
//  
//
//  Created by loki on 14.04.2021.
//

import Foundation

//public struct WTF : Error {
//    var localizedDescription : String
//
//    public init(_ desc: String) {
//        localizedDescription = desc
//    }
//}

public func WTF(_ msg: String, code: Int = 0) -> Error {
    NSError(code: code, message: msg)
}

internal extension NSError {
    convenience init(code: Int, message: String) {
        let userInfo: [String: String] = [NSLocalizedDescriptionKey:message]
        self.init(domain: "FTW", code: code, userInfo: userInfo)
    }
}

public extension Result {
    static func wtf(_ m: String, code: Int = 0) -> Result<Success,Error> {
        return .failure(WTF(m, code: code))
    }
}

public extension Result {
    static var notImplemented : Result<Success,Error> {
        return .wtf("something not implemented")
    }
    
    static func notImplemented(_ topic: String) -> Result<Success,Error> {
        return .wtf("\(topic) not implemented")
    }
}
