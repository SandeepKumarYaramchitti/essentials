import Foundation

@available(macOS 10.15.4, *)
@available(iOS 13.4, *)
public extension FileHandle {
    func close_r() -> R<FileHandle> {
        do {
            try self.close()
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    func seekToEnd_r() -> R<FileHandle> {
        do {
            try self.seekToEnd()
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    func write(_ data: Data) -> R<FileHandle> {
        do {
            try self.write(contentsOf: data)
            return .success(self)
        } catch {
            return .failure(error)
        }
    }
    
    func write(string: String) -> R<FileHandle> {
        string.asData() | { self.write($0) }
    }
}
