#if os(macOS)

import Foundation

@available(macOS 10.15, *)
public extension Encodable {
    func asJson() -> Result<String, Error>{
        JSONEncoder().try(self)
            .flatMap{ $0.asString() }
    }
    
    var asJsonISO8601 : Result<String, Error> {
        JSONEncoder.javaScriptISO8601().try(self)
            .flatMap{ $0.asString() }
    }
}

public extension String {
    func decodeFromJson<T>(type: T.Type) -> Result<T, Error> where T: Decodable {
        self.asData()
            .flatMap { JSONDecoder().try(type, from: $0) }
    }
}

///////////////////////////////
/// HELPERS
//////////////////////////////

@available(macOS 10.15, *)
fileprivate extension JSONEncoder {
    func `try`<T : Encodable>(_ value: T) -> Result<Output, Error> {
        do {
            return .success(try self.encode(value))
        } catch {
            return .failure(error)
        }
    }
}

fileprivate extension JSONDecoder {
    func `try`<T: Decodable>(_ t: T.Type, from data: Data) -> Result<T,Error> {
        do {
            return .success(try self.decode(t, from: data))
        } catch {
            return .failure(error)
        }
    }
}


/////////////////////
/// FOR SERG
////////////////////

@available(macOS 10.15, *)
public extension Array where Element : Encodable {
    var asJSONString : R<String> {
        JSONEncoder().try(self) | { $0.asString() }
    }
}

public extension String {
    var asJSONArray : R<[String]> {
        return asData() | { JSONDecoder().try([String].self, from: $0) }
    }
}
#endif
