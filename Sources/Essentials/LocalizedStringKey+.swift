import Foundation
import SwiftUI

@available(macOS 10.15, *)
public extension LocalizedStringKey {
    private var keyStr: String {
        return "\(self)".keyFromLocalizedStringKey
    }
    
    func localizedStr(locale: Locale = .current) -> String {
        if #available(macOS 12, *) {
            return String(localized: "\(self.keyStr)")
        } else {
            return String.localizedString(for: self.keyStr, locale: locale)
        }
    }
}

fileprivate extension String {
    static func localizedString(for key: String, locale: Locale = .current) -> String {
        let language = locale.languageCode
        
        guard let path = Bundle.main.path(forResource: language, ofType: "lproj"),
              let bundle = Bundle(path: path)
        else { return "Failed To Get Localized String" }
        
        let localizedString = NSLocalizedString(key, bundle: bundle, comment: "")
        
        return localizedString
    }
    
    var keyFromLocalizedStringKey: String {
        let comp2 = self.substring(from: 25).components(separatedBy:"\", hasFormatting")
        
        if comp2.count == 2 {
            return comp2.first!
        }
        
        return "failed to get stringKey"
    }
}
