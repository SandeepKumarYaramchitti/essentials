//
//  File.swift
//  
//
//  Created by UKS on 20.12.2021.
//

import Foundation

public extension String {
    var skippingLastSlash : String {
        if self.hasSuffix("/") {
            return String(self.dropLast())
        }
        
        return self
    }
    
    func removeFileExtension() -> String {
        for i in stride(from: self.count, to: 0, by: -1) {
            if i > 0 {
                if self[i] == "." && self[i-1] != "/" {
                    return substring(to: i)
                }
            }
        }
        
        return self
    }
    
    
    var lastPathComonent : String {
        String(self.split(separator: "/").last ?? "" )
    }
    
    var getExceptLastPathComponent: String {
        self.split(separator: "/").dropLast().joined(separator: "/")
    }
}
