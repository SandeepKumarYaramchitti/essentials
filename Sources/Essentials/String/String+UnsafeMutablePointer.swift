//
//  File.swift
//  
//
//  Created by UKS on 21.12.2021.
//

public extension UnsafeMutablePointer where Pointee == CChar {
    func asString() -> String {
        return String(cString: self )
    }
}
