#if os(macOS)
import Foundation
import CommonCrypto

public extension String {
    
    var sha512: Data? {
        guard let stringData = self.data(using: String.Encoding.utf8)
        else { return nil }
        
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA512_DIGEST_LENGTH))
        
        _ = withUnsafePointer(to: stringData) { stringBytes in
            CC_SHA512(stringBytes, CC_LONG(stringData.count), &digest)
        }
        
        return Data(bytes: &digest, count: Int(CC_SHA512_DIGEST_LENGTH))
    }

}
#endif
