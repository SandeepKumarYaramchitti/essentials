//
//  File.swift
//  
//
//  Created by UKS on 10.08.2021.
//

import Foundation

public extension String {
    func trim(_ char: Character) -> String {
        self.trimStart(char).trimEnd(char)
    }
    
    func trim(_ symbols: [Character] = [" ", "\t", "\r", "\n"]) -> String {
        self.trimStart(symbols).trimEnd(symbols)
    }
    
    func trimStart(_ char: Character) -> String {
        return trimStart([char])
    }
    
    func trimStart(_ symbols: [Character] = [" ", "\t", "\r", "\n"]) -> String {
        var startIndex = 0
        
        for char in self {
            if symbols.contains(char) {
                startIndex += 1
            }
            else {
                break
            }
        }
        
        if startIndex == 0 {
            return self
        }
        
        return String( self.substring(from: startIndex) )
    }
    
    func trimEnd(_ char: Character) -> String {
        return trimEnd([char])
    }
    
    func trimEnd(_ symbols: [Character] = [" ", "\t", "\r", "\n"]) -> String {
        var endIndex = self.count - 1
        
        if endIndex == -1 {
            return self
        }
        
        for i in (0...endIndex).reversed() {
            if symbols.contains( self[i] ) {
                endIndex -= 1
            }
            else {
                break
            }
        }
        
        if endIndex == self.count {
            return self
        }
        
        return String( self.substring(to: endIndex + 1) )
    }
}

/////////////////////////
/// ACCESS TO CHAR BY INDEX
////////////////////////
extension StringProtocol {
    subscript(offset: Int) -> Character { self[index(startIndex, offsetBy: offset)] }
    subscript(range: Range<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: ClosedRange<Int>) -> SubSequence {
        let startIndex = index(self.startIndex, offsetBy: range.lowerBound)
        return self[startIndex..<index(startIndex, offsetBy: range.count)]
    }
    subscript(range: PartialRangeFrom<Int>) -> SubSequence { self[index(startIndex, offsetBy: range.lowerBound)...] }
    subscript(range: PartialRangeThrough<Int>) -> SubSequence { self[...index(startIndex, offsetBy: range.upperBound)] }
    subscript(range: PartialRangeUpTo<Int>) -> SubSequence { self[..<index(startIndex, offsetBy: range.upperBound)] }
}
