#if os(macOS)
import Foundation

public extension XR {
    
    struct Shell {
        public let cmd : String
        public let workDir : URL
        
        public init(cmd: String, workDir: URL = URL.userHome) {
            self.cmd = cmd
            self.workDir = workDir
        }
        
        public func run(args: [String]?) -> Process {
            let standardOutputPipe = Pipe()
            let standardErrorPipe = Pipe()
            let task = Process()
            if #available(macOS 10.13, *) {
                task.currentDirectoryURL = workDir
            } else {
                // Fallback on earlier versions
            }
            task.standardOutput = standardOutputPipe
            task.standardError  = standardErrorPipe
            task.launchPath = cmd
            task.arguments = args
            task.launch()
            task.waitUntilExit()
            
            return task
        }
    }
}

public extension Process {
    var outputAsString : R<String> {
        if self.terminationStatus == 0 {
            
            if let pipe = standardOutput as? Pipe {
                return pipe.fileHandleForReading
                    .readDataToEndOfFile()
                    .asString()
            }
            return .wtf("Process: standardOutput as? Pipe == nil")
            
        } else {
            
            if let pipe = standardError as? Pipe {
                let data = pipe.fileHandleForReading.readDataToEndOfFile()
                if let errorDetails = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return .wtf("task failed : \(self.terminationReason)\nDetails: \(errorDetails)", code: Int(self.terminationStatus))
                }
            }
            
            return .wtf("Process: standardError as? Pipe == nil OR failed to get error details for some reason")
        }
    }
}
#endif

