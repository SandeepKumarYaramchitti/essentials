import XCTest
import Essentials

final class StringTruncTests: XCTestCase {
    func test_ShouldNotTruncate() {
        XCTAssert("12345".truncStart(length: 10) == "12345")
        
        XCTAssert("12345".truncEnd(length: 10) == "12345")
    }
    
    func test_ShouldTruncate() {
        let a = "12345678901234567890_1234567890".truncStart(length: 10)
        XCTAssert(a == "…1234567890")
        
        let b = "1234567890_12345678901234567890".truncEnd(length: 10)
        XCTAssert(b == "1234567890…")
        
        let c = "12345_67890123456789012345_67890".truncCenter(length: 10)
        XCTAssert(c == "12345…67890")
    }
    
    func test_IntIdxAccess() {
        let nums = "01234567890"
        XCTAssertEqual(nums[1], "1")
        XCTAssertEqual(nums[15], "")
    }
    
    func test_IntRangeIdxAccess() {
        let nums = "01234567890"
        
        XCTAssertEqual(nums[1...5].asString(), "12345")
        XCTAssertEqual(nums[1..<5].asString(), "1234")
        XCTAssertEqual("012"[1..<5].asString(), "12")
        XCTAssertEqual("012"[1...5].asString(), "12")
    }
}
